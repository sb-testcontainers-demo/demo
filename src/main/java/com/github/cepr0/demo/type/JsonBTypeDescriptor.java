package com.github.cepr0.demo.type;

import com.vladmihalcea.hibernate.type.json.internal.JsonTypeDescriptor;
import com.vladmihalcea.hibernate.type.util.ObjectMapperWrapper;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Objects;

public class JsonBTypeDescriptor extends JsonTypeDescriptor {

	public JsonBTypeDescriptor() {
		super();
	}

	public JsonBTypeDescriptor(final ObjectMapperWrapper objectMapperWrapper) {
		super(objectMapperWrapper);
	}

	public JsonBTypeDescriptor(final ObjectMapperWrapper objectMapperWrapper, final Type type) {
		super(objectMapperWrapper, type);
	}

	@Override
	public boolean areEqual(final Object one, final Object another) {
		if (one instanceof Collection && another instanceof Collection) {
			return Objects.equals(one, another);
		}
		if (one instanceof Serializable && another instanceof Serializable) {
			return Objects.equals(one, another);
		}
		return super.areEqual(one, another);
	}
}
