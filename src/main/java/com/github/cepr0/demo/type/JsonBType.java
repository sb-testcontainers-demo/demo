package com.github.cepr0.demo.type;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vladmihalcea.hibernate.type.json.internal.JsonBinarySqlTypeDescriptor;
import com.vladmihalcea.hibernate.type.util.Configuration;
import com.vladmihalcea.hibernate.type.util.ObjectMapperWrapper;
import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.usertype.DynamicParameterizedType;

import java.lang.reflect.Type;
import java.util.Properties;

public class JsonBType extends AbstractSingleColumnStandardBasicType<Object> implements DynamicParameterizedType {

	public static final JsonBType INSTANCE = new JsonBType();

	public JsonBType() {
		super(
				JsonBinarySqlTypeDescriptor.INSTANCE,
				new JsonBTypeDescriptor(Configuration.INSTANCE.getObjectMapperWrapper())
		);
	}

	public JsonBType(ObjectMapper objectMapper) {
		super(
				JsonBinarySqlTypeDescriptor.INSTANCE,
				new JsonBTypeDescriptor(new ObjectMapperWrapper(objectMapper))
		);
	}

	public JsonBType(ObjectMapperWrapper objectMapperWrapper) {
		super(
				JsonBinarySqlTypeDescriptor.INSTANCE,
				new JsonBTypeDescriptor(objectMapperWrapper)
		);
	}

	public JsonBType(ObjectMapper objectMapper, Type javaType) {
		super(
				JsonBinarySqlTypeDescriptor.INSTANCE,
				new JsonBTypeDescriptor(new ObjectMapperWrapper(objectMapper), javaType)
		);
	}

	public JsonBType(ObjectMapperWrapper objectMapperWrapper, Type javaType) {
		super(
				JsonBinarySqlTypeDescriptor.INSTANCE,
				new JsonBTypeDescriptor(objectMapperWrapper, javaType)
		);
	}

	@Override
	public String getName() {
		return "jsonb";
	}

	@Override
	public void setParameterValues(final Properties parameters) {
		((JsonBTypeDescriptor) getJavaTypeDescriptor()).setParameterValues(parameters);
	}
}
