package com.github.cepr0.demo.domain.user.dto;

import com.github.cepr0.crud.dto.CrudResponse;
import com.github.cepr0.demo.model.Role;
import lombok.Builder;
import lombok.Value;

import java.util.Set;

@Value
@Builder
public class UserResponse implements CrudResponse<Long> {
	/**
	 * ID of the User
	 */
	private Long id;

	/**
	 * A name of the User
	 */
	private String name;

	/**
	 * List of the User phones
	 */
	private Set<String> phones;

	/**
	 * User roles
	 */
	private Set<Role> roles;
}
