package com.github.cepr0.demo.domain.user.dto;

import com.github.cepr0.crud.dto.CrudRequest;
import com.github.cepr0.demo.model.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest implements CrudRequest {
	/**
	 * A name of the User
	 */
	@NotBlank
	private String name;

	/**
	 * List of the User phones
	 */
	@NotNull
	@Size(min = 1, max = 10)
	private Set<String> phones;

	/**
	 * User roles
	 */
	@NotNull
	@Size(min = 1, max = 2)
	private Set<Role> roles;
}
