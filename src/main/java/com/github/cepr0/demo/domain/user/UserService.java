package com.github.cepr0.demo.domain.user;

import com.github.cepr0.crud.service.AbstractCrudService;
import com.github.cepr0.demo.domain.user.dto.UserRequest;
import com.github.cepr0.demo.domain.user.dto.UserResponse;
import com.github.cepr0.demo.model.User;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
public class UserService extends AbstractCrudService<User, Long, UserRequest, UserResponse> {
	public UserService(@NonNull final UserRepo repo, @NonNull final UserMapper mapper) {
		super(repo, mapper);
	}
}
