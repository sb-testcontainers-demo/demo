package com.github.cepr0.demo.domain.user;

import com.github.cepr0.crud.mapper.CrudMapper;
import com.github.cepr0.demo.domain.user.dto.UserRequest;
import com.github.cepr0.demo.domain.user.dto.UserResponse;
import com.github.cepr0.demo.model.User;
import org.mapstruct.Mapper;

import static org.mapstruct.NullValueCheckStrategy.ALWAYS;
import static org.mapstruct.NullValueMappingStrategy.RETURN_DEFAULT;
import static org.mapstruct.NullValuePropertyMappingStrategy.IGNORE;

@Mapper(
		nullValueCheckStrategy = ALWAYS,
		nullValueMappingStrategy = RETURN_DEFAULT,
		nullValuePropertyMappingStrategy = IGNORE
)
public abstract class UserMapper implements CrudMapper<User, Long, UserRequest, UserResponse> {
}
