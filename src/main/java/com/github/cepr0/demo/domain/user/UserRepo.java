package com.github.cepr0.demo.domain.user;

import com.github.cepr0.crud.repo.CrudRepo;
import com.github.cepr0.demo.model.User;

public interface UserRepo extends CrudRepo<User, Long> {
}
