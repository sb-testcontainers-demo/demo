package com.github.cepr0.demo.model.base;

import com.github.cepr0.crud.model.IdentifiableEntity;

import java.io.Serializable;

public abstract class BaseEntity<ID extends Serializable> implements IdentifiableEntity<ID> {

	@Override
	public String toString() {
		return getClass().getSimpleName() + "{id=" + getId() + '}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		return getId() != null && getId().equals(((BaseEntity) o).getId());
	}

	@Override
	public int hashCode() {
		return 31;
	}
}
