package com.github.cepr0.demo.model.base;

import com.github.cepr0.demo.type.JsonBType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
@TypeDef(name = "jsonb", typeClass = JsonBType.class)
public abstract class LongIdEntity extends BaseEntity<Long> {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Version private Integer version;
}
