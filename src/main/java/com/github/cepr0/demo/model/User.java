package com.github.cepr0.demo.model;

import com.github.cepr0.demo.model.base.LongIdEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
@Entity
@Table(name = "users")
@DynamicInsert
@DynamicUpdate
public class User extends LongIdEntity {

	@Column(nullable = false)
	private String name;

	@Type(type = "jsonb")
	private Set<String> phones;

	@Column(nullable = false)
	@Type(type = "jsonb")
	private Set<Role> roles;

	@Builder
	public User(final Long id, final String name, Set<String> phones, final Set<Role> roles) {
		setId(id);
		this.phones = phones;
		this.name = name;
		this.roles = roles;
	}
}
