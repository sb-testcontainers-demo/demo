package com.github.cepr0.demo.api;

import com.github.cepr0.crud.controller.AbstractCrudController;
import com.github.cepr0.demo.domain.user.UserService;
import com.github.cepr0.demo.domain.user.dto.UserRequest;
import com.github.cepr0.demo.domain.user.dto.UserResponse;
import com.github.cepr0.demo.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("users")
public class UserController extends AbstractCrudController<User, Long, UserRequest, UserResponse> {

	public UserController(@NonNull final UserService service) {
		super(service);
	}

	/**
	 * Create a new User.
	 */
	@PostMapping
	@Override
	public UserResponse create(@Validated @RequestBody @NonNull final UserRequest request) {
		return super.create(request);
	}

	/**
	 * Update an existing User.
	 *
	 * @param id ID of the User being updated.
	 */
	@PatchMapping("/{id}")
	@Override
	public ResponseEntity<?> update(@PathVariable("id") @NonNull final Long id, @Validated @RequestBody @NonNull final UserRequest request) {
		return super.update(id, request);
	}

	/**
	 * Delete an existing User.
	 *
	 * @param id ID of the User being deleted.
	 */
	@DeleteMapping("/{id}")
	@Override
	public ResponseEntity<?> delete(@PathVariable("id") @NonNull final Long id) {
		return super.delete(id);
	}

	/**
	 * Get an existing User.
	 *
	 * @param id ID of the User being deleted.
	 */
	@GetMapping("/{id}")
	@Override
	public ResponseEntity<?> getOne(@PathVariable("id") @NonNull final Long id) {
		return super.getOne(id);
	}

	/**
	 * Get all Users.
	 */
	@GetMapping
	@Override
	public List<UserResponse> getAll() {
		return super.getAll();
	}
}
