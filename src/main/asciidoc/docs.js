document.addEventListener('DOMContentLoaded', () => {
	(function() {
		const button = document.createElement('a');
		const aside = document.querySelector('div.toc2');

		button.setAttribute('href', "./index.html");
		button.setAttribute('id', "toctitle");
		button.setAttribute('class', "link-comeback-to-home");
		aside.appendChild(button);
	})()	
})