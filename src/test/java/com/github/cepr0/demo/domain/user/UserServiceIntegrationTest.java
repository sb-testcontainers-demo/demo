package com.github.cepr0.demo.domain.user;

import com.github.cepr0.demo.domain.user.dto.UserRequest;
import com.github.cepr0.demo.domain.user.dto.UserResponse;
import com.integralblue.log4jdbc.spring.Log4jdbcAutoConfiguration;
import org.flywaydb.core.Flyway;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static com.github.cepr0.demo.SqlCountersLogDelegator.*;
import static com.github.cepr0.demo.model.Role.ADMIN;
import static com.github.cepr0.demo.model.Role.USER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;
import static org.springframework.transaction.annotation.Propagation.NOT_SUPPORTED;

@RunWith(SpringRunner.class)
@DataJpaTest(showSql = false)
@Transactional(propagation = NOT_SUPPORTED)
@AutoConfigureTestDatabase(replace = NONE)
@ActiveProfiles("tc")
@ComponentScan("com.github.cepr0.demo.domain.user")
@Import(Log4jdbcAutoConfiguration.class)
public class UserServiceIntegrationTest {

	private static final String USER_NAME = "user";
	private static final String USER_NAME_UPDATED = "user_updated";

	@Autowired private UserService userService;
	@Autowired Flyway flyway;

	@Before
	public void setUp() {
		flyway.clean();
		flyway.baseline();
		flyway.migrate();
	}

	@Test
	public void create() {
		resetSqlCounters();

		assertThat(userService.getAll()).isEmpty();

		UserResponse response = userService.create(UserRequest.builder()
				.name(USER_NAME)
				.phones(Set.of("+1234567890", "+789456123"))
				.roles(Set.of(USER, ADMIN))
				.build()
		);
		assertThat(response.getId()).isEqualTo(1L);
		assertThat(response.getName()).isEqualTo(USER_NAME);

		assertThat(userService.getAll()).hasSize(1);

		assertSelectCount(2);
		assertInsertCount(1);
	}

	@Test
	public void update() {
		resetSqlCounters();
		assertThat(userService.getAll()).isEmpty();

		Long userId = userService.create(UserRequest.builder()
				.name(USER_NAME)
				.phones(Set.of("+1234567890", "+789456123"))
				.roles(Set.of(USER, ADMIN))
				.build()
		).getId();

		assertThat(userService.update(userId, UserRequest.builder()
				.name(USER_NAME_UPDATED)
				.phones(Set.of("+456789132", "+159478236"))
				.roles(Set.of(USER, ADMIN))
				.build()))
				.isNotEmpty()
				.map(UserResponse::getName)
				.hasValue(USER_NAME_UPDATED);

		assertThat(userService.getAll()).hasSize(1);

		assertSelectCount(3);
		assertInsertCount(1);
		assertUpdateCount(1);
	}
}