package com.github.cepr0.demo.api;

import com.github.cepr0.demo.domain.user.UserService;
import com.github.cepr0.demo.domain.user.dto.UserRequest;
import com.github.cepr0.demo.domain.user.dto.UserResponse;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Set;

import static capital.scalable.restdocs.AutoDocumentation.responseFields;
import static com.github.cepr0.demo.SqlCountersLogDelegator.*;
import static com.github.cepr0.demo.model.Role.ADMIN;
import static com.github.cepr0.demo.model.Role.USER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("tc")
@ComponentScan("com.github.cepr0.demo")
public class UserControllerIntegrationTest extends AbstractWebTest {

	private static final String USERS = "/users";
	private static final String USER_NAME = "user";
	private static final String USER_NAME_UPDATED = "user_updated";

	@Autowired private UserService userService;

	// Create User
	// -----------

	@Test
	public void create_when_all_is_correct_then_201_Created() throws Exception {
		resetSqlCounters();

		ResultActions result = mockMvc.perform(post(USERS)
				.contentType(APPLICATION_JSON_UTF8)
				.content(toJson(UserRequest
						.builder()
						.name(USER_NAME)
						.phones(Set.of("+1234567890", "+789456123"))
						.roles(Set.of(USER, ADMIN))
						.build()
				))
				.accept(APPLICATION_JSON)
		);

		result.andExpect(status().isCreated())
				.andExpect(content().contentType(APPLICATION_JSON_UTF8));

		assertBody(result, UserResponse
				.builder()
				.name(USER_NAME)
				.phones(Set.of("+1234567890", "+789456123"))
				.roles(Set.of(USER, ADMIN))
				.build(), "id")
				.satisfies(body -> assertThat(body.getId()).isNotNull());

		result.andDo(doc("create/1",
				requestHeaders(ACCEPT_HEADER, CONTENT_TYPE_HEADER),
				responseHeaders(CONTENT_TYPE_HEADER))
		);

		assertInsertCount(1);
	}

	@Test
	public void create_when_name_is_blank_then_400_Bad_Request() throws Exception {
		ResultActions result = mockMvc.perform(post(USERS)
				.contentType(APPLICATION_JSON_UTF8)
				.content(toJson(UserRequest
						.builder()
						.name(" ")
						.phones(Set.of("+1234567890", "+789456123"))
						.roles(Set.of(USER, ADMIN))
						.build()
				))
				.accept(APPLICATION_JSON)
		);

		result.andExpect(status().isBadRequest());

		result.andDo(doc("create/2",
				requestHeaders(ACCEPT_HEADER, CONTENT_TYPE_HEADER))
		);
	}

	@Test
	public void create_when_name_is_empty_then_400_Bad_Request() throws Exception {
		ResultActions result = mockMvc.perform(post(USERS)
				.contentType(APPLICATION_JSON_UTF8)
				.content(toJson(UserRequest
						.builder()
						.name("")
						.phones(Set.of("+1234567890", "+789456123"))
						.roles(Set.of(USER, ADMIN))
						.build()
				))
				.accept(APPLICATION_JSON)
		);

		result.andExpect(status().isBadRequest());
	}

	@Test
	public void create_when_name_is_null_then_400_Bad_Request() throws Exception {
		ResultActions result = mockMvc.perform(post(USERS)
				.contentType(APPLICATION_JSON_UTF8)
				.content(toJson(new UserRequest()))
				.accept(APPLICATION_JSON)
		);

		result.andExpect(status().isBadRequest());
	}

	// Updated User
	// ------------

	@Test
	public void update_when_all_is_correct_then_200_Ok() throws Exception {
		resetSqlCounters();

		UserResponse user = userService.create(UserRequest
				.builder()
				.name(USER_NAME)
				.phones(Set.of("+1234567890", "+789456123"))
				.roles(Set.of(USER, ADMIN))
				.build()
		);

		ResultActions result = mockMvc.perform(patch(USERS + "/{id}", user.getId())
				.accept(APPLICATION_JSON)
				.contentType(APPLICATION_JSON_UTF8)
				.content(toJson(UserRequest
						.builder()
						.name(USER_NAME_UPDATED)
						.phones(Set.of("+789456123", "+1234567890"))
						.roles(Set.of(USER, ADMIN))
						.build()
				))
		);

		result.andExpect(status().isOk())
				.andExpect(content().contentType(APPLICATION_JSON_UTF8));

		assertBody(result, UserResponse
				.builder()
				.id(user.getId())
				.name(USER_NAME_UPDATED)
				.phones(Set.of("+1234567890", "+789456123"))
				.roles(Set.of(USER, ADMIN))
				.build()
		);

		result.andDo(doc("update/1",
				requestHeaders(ACCEPT_HEADER, CONTENT_TYPE_HEADER),
				responseHeaders(CONTENT_TYPE_HEADER),
				responseFields().responseBodyAsType(UserResponse.class)
		));

		assertSelectCount(1);
		assertInsertCount(1);
		assertUpdateCount(1);
	}

	@Test
	public void update_when_name_is_blank_then_400_Bad_Request() throws Exception {
		UserResponse user = userService.create(UserRequest
				.builder()
				.name(USER_NAME)
				.roles(Set.of(USER, ADMIN))
				.build()
		);

		ResultActions result = mockMvc.perform(patch(USERS + "/{id}", user.getId())
				.accept(APPLICATION_JSON)
				.contentType(APPLICATION_JSON_UTF8)
				.content(toJson(UserRequest
						.builder()
						.name(" ")
						.roles(Set.of(USER, ADMIN))
						.build()
				))
		);

		result.andExpect(status().isBadRequest());

		result.andDo(doc("update/2",
				requestHeaders(ACCEPT_HEADER, CONTENT_TYPE_HEADER))
		);
	}

}