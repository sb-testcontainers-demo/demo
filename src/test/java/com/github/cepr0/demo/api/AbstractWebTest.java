package com.github.cepr0.demo.api;

import capital.scalable.restdocs.AutoDocumentation;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.ObjectAssert;
import org.flywaydb.core.Flyway;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.lang.NonNull;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.headers.HeaderDescriptor;
import org.springframework.restdocs.headers.HeaderDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.snippet.Snippet;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static capital.scalable.restdocs.jackson.JacksonResultHandlers.prepareJackson;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.snippet.Attributes.key;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureDataJpa
public abstract class AbstractWebTest {

	protected static final HeaderDescriptor CONTENT_TYPE_HEADER = HeaderDocumentation
			.headerWithName("Content-Type")
			.description(APPLICATION_JSON_UTF8_VALUE)
			.attributes(key("type").value("String"));
	protected static final HeaderDescriptor ACCEPT_HEADER = HeaderDocumentation
			.headerWithName("Accept")
			.description(APPLICATION_JSON)
			.attributes(key("type").value("String"));


	@Autowired protected Flyway flyway;
	@Autowired protected ObjectMapper objectMapper;
	@Autowired protected WebApplicationContext context;

	protected MockMvc mockMvc;

	@Rule
	public JUnitRestDocumentation restDoc = new JUnitRestDocumentation();

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
				.alwaysDo(prepareJackson(objectMapper))
				.apply(documentationConfiguration(restDoc)
						.operationPreprocessors()
						.withRequestDefaults(prettyPrint())
						.withResponseDefaults(prettyPrint())
						.and()
						.snippets()
						.withDefaults(defaultSnippets())
				)
				.build();

		flyway.clean();
		flyway.baseline();
		flyway.migrate();
	}

	protected Snippet[] defaultSnippets() {
		return new Snippet[]{
				PayloadDocumentation.requestBody(),
				PayloadDocumentation.responseBody(),
				CliDocumentation.curlRequest(),
				HttpDocumentation.httpRequest(),
				HttpDocumentation.httpResponse(),
				AutoDocumentation.requestFields(),
				AutoDocumentation.responseFields(),
				AutoDocumentation.pathParameters(),
				AutoDocumentation.requestParameters(),
				AutoDocumentation.description(),
				AutoDocumentation.methodAndPath(),
				AutoDocumentation.requestHeaders()
		};
	}

	protected RestDocumentationResultHandler doc(@NonNull final String path, Snippet... snippets) {
		return document("{class-name}/" + path, snippets);
	}

	protected String toJson(Object object) throws JsonProcessingException {
		return objectMapper.writeValueAsString(object);
	}

	protected <T> ObjectAssert<T> assertBody(ResultActions result, T expected, String... ignoredFields) throws Exception {
		String body = result.andReturn().getResponse().getContentAsString();
		//noinspection unchecked
		return (ObjectAssert<T>) assertThat(objectMapper.readValue(body, expected.getClass()))
				.isEqualToIgnoringGivenFields(expected, ignoredFields);
	}
}
