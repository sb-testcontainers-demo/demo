package com.github.cepr0.demo;

import lombok.extern.slf4j.Slf4j;
import net.sf.log4jdbc.log.slf4j.Slf4jSpyLogDelegator;
import net.sf.log4jdbc.sql.Spy;
import org.assertj.core.api.AbstractIntegerAssert;

import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class SqlCountersLogDelegator extends Slf4jSpyLogDelegator {

	private static final AtomicInteger SELECT_COUNTER = new AtomicInteger();
	private static final AtomicInteger INSERT_COUNTER = new AtomicInteger();
	private static final AtomicInteger UPDATE_COUNTER = new AtomicInteger();
	private static final AtomicInteger DELETE_COUNTER = new AtomicInteger();

	private static final String SELECT_TEMPLATE = ".*select .+ from .+";
	private static final String INSERT_TEMPLATE = "(.*\\s)?.*insert into .+ values.+";
	private static final String UPDATE_TEMPLATE = "(.*\\s)?.*update .+ set.+";
	private static final String DELETE_TEMPLATE = "(.*\\s)?.*delete from .+";

	@Override
	public void sqlOccurred(final Spy spy, final String methodCall, final String sql) {
		switch (operationOf(sql)) {
			case SELECT:
				SELECT_COUNTER.incrementAndGet();
				break;
			case INSERT:
				INSERT_COUNTER.incrementAndGet();
				break;
			case UPDATE:
				UPDATE_COUNTER.incrementAndGet();
				break;
			case DELETE:
				DELETE_COUNTER.incrementAndGet();
				break;
		}
		super.sqlOccurred(spy, methodCall, sql);
	}

	public static void resetSqlCounters() {
		SELECT_COUNTER.set(0);
		INSERT_COUNTER.set(0);
		UPDATE_COUNTER.set(0);
		DELETE_COUNTER.set(0);
	}

	@SuppressWarnings("UnusedReturnValue")
	public static AbstractIntegerAssert<?> assertSelectCount(int expected) {
		return assertThat(SELECT_COUNTER.get())
				.as("The number of select statements")
				.isEqualTo(expected);
	}

	@SuppressWarnings("UnusedReturnValue")
	public static AbstractIntegerAssert<?> assertInsertCount(int expected) {
		return assertThat(INSERT_COUNTER.get())
				.as("The number of insert statements")
				.isEqualTo(expected);
	}

	@SuppressWarnings("UnusedReturnValue")
	public static AbstractIntegerAssert<?> assertUpdateCount(int expected) {
		return assertThat(UPDATE_COUNTER.get())
				.as("The number of update statements")
				.isEqualTo(expected);
	}

	@SuppressWarnings("UnusedReturnValue")
	public static AbstractIntegerAssert<?> assertDeleteCount(int expected) {
		return assertThat(DELETE_COUNTER.get())
				.as("The number of delete statements")
				.isEqualTo(expected);
	}

	private SqlOperation operationOf(final String sql) {
		if (sql == null) return SqlOperation.UNDEFINED;

		if (sql.toLowerCase().matches(SELECT_TEMPLATE)) {
			return SqlOperation.SELECT;
		}

		if (sql.toLowerCase().matches(INSERT_TEMPLATE)) {
			return SqlOperation.INSERT;
		}

		if (sql.toLowerCase().matches(UPDATE_TEMPLATE)) {
			return SqlOperation.UPDATE;
		}

		if (sql.toLowerCase().matches(DELETE_TEMPLATE)) {
			return SqlOperation.DELETE;
		}

		return SqlOperation.UNDEFINED;
	}

	public enum SqlOperation {
		SELECT, INSERT, UPDATE, DELETE, UNDEFINED
	}
}
