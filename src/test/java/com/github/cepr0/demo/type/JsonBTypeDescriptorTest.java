package com.github.cepr0.demo.type;

import com.vladmihalcea.hibernate.type.util.ObjectMapperWrapper;
import lombok.Value;
import org.junit.Test;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

public class JsonBTypeDescriptorTest {
	private JsonBTypeDescriptor descriptor = new JsonBTypeDescriptor(new ObjectMapperWrapper());

	@Test
	public void areEqual() {
		Set<String> set1 = Set.of("one", "two");
		Set<String> set2 = Set.of("two", "one");
		assertTrue(descriptor.areEqual(set1, set2));

		Set<Some> some1 = Set.of(Some.ONE, Some.TWO);
		Set<Some> some2 = Set.of(Some.TWO, Some.ONE);
		assertTrue(descriptor.areEqual(some1, some2));

		String str1 = new String("one");
		String str2 = new String("one");
		assertTrue(descriptor.areEqual(str1, str2));

		Map<String, String> map1 = Map.of("key2", "value2", "key1", "value1" );
		Map<String, String> map2 = Map.of("key1", "value1", "key2", "value2" );
		assertTrue(descriptor.areEqual(map1, map2));

		Simple simple1 = new Simple("name", 10);
		Simple simple2 = new Simple("name", 10);
		assertTrue(descriptor.areEqual(simple1, simple2));

		Complex complex1 = new Complex(
				"name",
				Set.of("123456", "456789"),
				Map.of(
						"key1", "value1",
						"key2", "valaue2"
				),
				Set.of(
						new Simple("name1", 10),
						new Simple("name2", 11)
				)
		);

		Complex complex2 = new Complex(
				"name",
				Set.of("456789", "123456"),
				Map.of(
						"key2", "valaue2",
						"key1", "value1"
				),
				Set.of(
						new Simple("name2", 11),
						new Simple("name1", 10)
				)
		);
		assertThat(descriptor.areEqual(complex1, complex2)).isTrue();

		List<String> list1 = List.of("one", "two");
		List<String> list2 = List.of("one", "two");
		assertThat(descriptor.areEqual(list1, list2)).isTrue();

	}

	enum Some {
		ONE, TWO
	}

	@Value
	private class Simple {
		private String name;
		private Integer age;
	}

	@Value
	private class Complex implements Serializable {
		private String name;
		private Set<String> phones;
		private Map<String, String> data;
		private Set<Simple> simples;
	}
}