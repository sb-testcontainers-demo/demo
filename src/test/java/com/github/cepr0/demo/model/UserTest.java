package com.github.cepr0.demo.model;

import org.assertj.core.api.Condition;
import org.junit.Test;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.EnumSet;
import java.util.Set;

import static com.github.cepr0.demo.model.Role.ADMIN;
import static com.github.cepr0.demo.model.Role.USER;
import static org.assertj.core.api.Assertions.*;

public class UserTest {

	@Test
	public void map() {
		assertThat(Set.of(
				User.builder().name("user1").build(),
				User.builder().name("user2").build()
		)).hasSize(2);

		User user3 = User.builder().id(3L).name("user3").build();
		User user4 = User.builder().id(4L).name("user4").build();

		assertThat(Set.of(user3, user4)).hasSize(2);

		assertThatThrownBy(() -> Set.of(user3, user3))
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessageStartingWith("duplicate element");
	}

	@Test
	public void conditions() {
		Condition<User> nameJohn = new Condition<>(user -> user.getName().equalsIgnoreCase("John"), "name=John");
		Condition<User> id1 = new Condition<>(user -> Long.valueOf(1L).equals(user.getId()), "id=1L");

		User john = User.builder().id(1L).name("John").build();

		assertThat(john).is(allOf(nameJohn, id1));

		assertThat(john).satisfies(user -> {
			assertThat(user.getId()).isEqualTo(1L);
			assertThat(user.getName()).isEqualToIgnoringCase("John");
		});
	}

	@Test
	public void equality() {
		User user1 = User.builder().id(1L).name("user1").build();
		User user2 = User.builder().id(1L).name("user2").build();
		assertThat(user1).isEqualTo(user2);

		User user3 = User.builder().name("user").build();
		User user4 = User.builder().name("user").build();
		assertThat(user3).isEqualToIgnoringGivenFields(user4, "id");
	}

	@Test
	public void set() throws IOException {
		EnumSet<Role> roles1 = EnumSet.of(USER, ADMIN);
		EnumSet<Role> roles2 = EnumSet.of(ADMIN, USER);
		assertThat(roles2).isEqualTo(roles1);

		User user1 = User.builder().id(1L).name("user").roles(roles1).build();
		User user2 = User.builder().id(1L).name("user").roles(roles2).build();
		assertThat(user1).isEqualToComparingFieldByField(user2);

		ObjectMapper mapper = new ObjectMapper();
		String jsonUser1 = mapper.writeValueAsString(user1);
		String jsonUser2 = mapper.writeValueAsString(user2);

		User user3 = mapper.readValue(jsonUser1, User.class);
		User user4 = mapper.readValue(jsonUser2, User.class);
		assertThat(user3).isEqualToComparingFieldByField(user4);
	}
}